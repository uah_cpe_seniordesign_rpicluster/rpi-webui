import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {interval, Observable, Subject} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private host: string;

  public poll$: Observable<any>;
  private pollSubject = new Subject<any>();
  private currentHash: string;


  constructor(private http: HttpClient) {
    this.host = '127.0.0.1:8081';
    this.currentHash = '';
    this.poll$ = this.pollSubject.asObservable();
    interval(5000).pipe(switchMap(() =>
      this.getDataHash())).subscribe((data) =>
      this.onPoll(data));
  }

  private onPoll(data) {
    if (this.currentHash !== data) {
      console.log('polling: C:' + this.currentHash + ', N: ' + data);
      this.currentHash = data;
      this.getHostList().subscribe((hosts) => this.pollNotifier(hosts));
    }
  }
  public pollNotifier(hosts) {
    this.pollSubject.next(hosts);
  }
  public setHost(h: string) {
    this.host = 'http://' + h + '/api/';
  }
  public getDataHash(): Observable<any> {
    return this.http.get(this.host + 'updateHash', {responseType: 'text'});
  }
  public getHello(): Observable<any> {
    return this.http.get(this.host + 'hello', {responseType: 'text'});
  }
  public getHostList(): Observable<any> {
    return this.http.get(this.host + 'getHostsList');
  }
  public getHostData(host: string): Observable<any> {
    return this.http.get(this.host + 'getHostData?host=' + host);
  }
  public getAllHostData(): Observable<any> {
    return this.http.get(this.host + 'getAllHostData');
  }
}

