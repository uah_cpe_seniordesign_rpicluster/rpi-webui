import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {
  private hostIp: string;
  private hostPort: string;
  private activeTheme: string;

  constructor() { }

  public getHostIp(): string {
    return this.hostIp;
  }
  public setHostIp(ip: string) {
    this.hostIp = ip;
  }
  public getHostPort(): string {
    return this.hostPort;
  }
  public setHostPort(port: string) {
    this.hostPort = port;
  }
  public getActiveTheme(): string {
    return this.activeTheme;
  }
  public setActiveTheme(theme: string) {
    this.activeTheme = theme;
  }
}
