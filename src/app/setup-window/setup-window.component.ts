import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {jqxWindowComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import {LocalStorageService} from 'ngx-localstorage';
import {DataService} from '../data.service';
import {ConfigurationService} from '../configuration.service';

@Component({
  selector: 'app-setup-window',
  templateUrl: './setup-window.component.html',
  styleUrls: ['./setup-window.component.css']
})
export class SetupWindowComponent implements AfterViewInit, OnInit {

  @ViewChild('configDiag') confDiag: jqxWindowComponent;

  public context: string;
  public activeTheme: string;
  private isConfigured: boolean;
  public hostIp: string;
  public hostPort: string;

  constructor(private cookieService: LocalStorageService,
              private dataService: DataService,
              private config: ConfigurationService) {

    if (this.keyExists('datasrvIp') && this.keyExists('datasrvPort')) {

      this.isConfigured = true;
      this.hostIp = this.cookieService.get('datasrvIp');
      this.hostPort = this.cookieService.get('datasrvPort');
      this.config.setHostIp(this.hostIp);
      this.config.setHostPort(this.hostPort);

      if (this.keyExists('theme')) {
        this.config.setActiveTheme(this.cookieService.get('theme'));
      } else {
        this.cookieService.set('theme', 'classic');
        this.config.setActiveTheme('classic');
      }
      this.dataService.setHost(this.hostIp + ':' + this.hostPort);
      this.testService();
    } else {
      this.isConfigured = false;
    }
  }
  private keyExists(key: string) {
    return this.cookieService.get(key) !== null && this.cookieService.get(key) !== 'undefined';
  }
  private testService() {
    this.dataService.getHello().subscribe((data) => {
      console.log('Got from remote: ' + data);
      if (data !== 'Hello World!') {
        this.isConfigured = false;
        this.context = 'Failed to contact service.';
      } else {
        this.cookieService.set('datasrvIp', this.hostIp);
        this.cookieService.set('datasrvPort', this.hostPort);
        this.confDiag.close();
      }
    });
  }

  ngAfterViewInit() {
    if (!this.isConfigured) {
      this.confDiag.open();
    }
  }
  ngOnInit() {
    this.activeTheme = this.cookieService.get('theme');
  }
  public onSubmit() {
    console.log('here');
    this.dataService.setHost(this.hostIp + ':' + this.hostPort);
    this.testService();
  }
}
