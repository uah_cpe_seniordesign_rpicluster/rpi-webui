import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupWindowComponent } from './setup-window.component';

describe('SetupWindowComponent', () => {
  let component: SetupWindowComponent;
  let fixture: ComponentFixture<SetupWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
