import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {jqxChartComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxchart';
import {ConfigurationService} from '../configuration.service';
import {DataService} from '../data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  @ViewChild('cpuTotalChart') cpuChart: jqxChartComponent;
  @ViewChild('memoryTotalChart') memChart: jqxChartComponent;
  @ViewChild('netTotalChart') netChart: jqxChartComponent;
  @ViewChild('thermalTotalChart') thmChart: jqxChartComponent;

  public activeTheme: string;

  public totalCpuUtilizationSource = [];
  public totalMemUtilizationSource = [];
  public totalNetUtilizationSource = [];
  public totalThmUtilizationSource = [];

  private multi = [];

  private currentTime;

  private rootSubscriber;

  constructor(private config: ConfigurationService,
              private data: DataService) { }

  ngOnInit() {
    this.activeTheme = this.config.getActiveTheme();
    this.start();
  }

  ngAfterViewInit() {
    this.cpuChart.seriesGroups([ this.getTotCpuOptions() ]);
    this.cpuChart.source(this.totalCpuUtilizationSource);
    this.cpuChart.setOptions(this.getOptions());

    this.memChart.seriesGroups([ this.getTotMemOptions() ]);
    this.memChart.source(this.totalMemUtilizationSource);
    this.memChart.setOptions(this.getOptions());

    this.netChart.seriesGroups([ this.getTotNetOptions() ]);
    this.netChart.source(this.totalNetUtilizationSource);
    this.netChart.setOptions(this.getOptions());

    this.thmChart.seriesGroups([ this.getTotThmOptions() ]);
    this.thmChart.source(this.totalThmUtilizationSource);
    this.thmChart.setOptions(this.getOptions());
  }

  public start() {
    console.log('starting subscriber for dashboard polling.');
    this.rootSubscriber = this.data.poll$.subscribe((data) => this.updater(data));
  }

  public end() {
    console.log('stopping subscriber for dashboard polling.');
    this.rootSubscriber.unsubscribe();
  }

  private isAlreadyThere(host: string): boolean {
    for (let i = 0; i < this.multi.length; i++) {
      const tempObj = this.multi[i];
      if (tempObj.dataField === host) {
        return true;
      }
    }
    return false; // If not returned by now, must be false
  }

  private getCurrentTimeString(): string {
    const d = new Date();
    return d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
  }

  private getOptions(): any {
    return { enableAnimations: false };
  }

  private getTotMemOptions(): any {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        maxValue: 100,
        minValue: 0,
        valuesOnTicks: true,
        description: '% Memory Utilization'
      },
      series: this.multi
    };
  }

  private getTotCpuOptions(): any {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        maxValue: 100,
        minValue: 0,
        valuesOnTicks: true,
        description: '% CPU Utilization'
      },
      series: this.multi
    };
  }

  private getTotNetOptions(): any {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        minValue: 0,
        valuesOnTicks: true,
        description: 'Received kB/s'
      },
      series: this.multi
    };
  }

  private getTotThmOptions(): any {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        minValue: 0,
        valuesOnTicks: true,
        description: 'CPU Temperature (C)'
      },
      series: this.multi
    };
  }

  private updater(data) {
    this.currentTime = this.getCurrentTimeString();
    for (let i = 0; i < data.length; i++) {
      this.data.getHostData(data[i]).subscribe((dat) => this.addDataForHost(dat));
    }
  }

  private removeIfNecessary() {

    if (this.totalCpuUtilizationSource.length < 60) {
      return;
    }
    this.totalCpuUtilizationSource.splice(0, 1);
    this.totalThmUtilizationSource.splice(0, 1);
    this.totalNetUtilizationSource.splice(0, 1);
    this.totalMemUtilizationSource.splice(0, 1);
  }

  private addDataForHost(data) {
    const multiObj = {dataField: data.hostname, displayText: data.hostname, emptyPointsDisplay: 'connect'};
    if (!this.isAlreadyThere(data.hostname)) { // Check if there is a dedicated series for the host. If not, create one.
      this.multi.push(multiObj);
    }
    /* CPU Shit */
    const objCpu = { };
    objCpu['Time'] = this.currentTime;
    objCpu[data.hostname] = data.totalCpuUtilization;
    this.totalCpuUtilizationSource.push(objCpu);

    /* Memory Shit */
    const objMem = { };
    objMem['Time'] = this.currentTime;
    objMem[data.hostname] = (data.physicalMemoryUsed / data.physicalMemoryTotal) * 100.0;
    this.totalMemUtilizationSource.push(objMem);

    /* Network Bandwidth Shit */
    const objNet = { };
    objNet['Time'] = this.currentTime;
    objNet[data.hostname] = data.rxBytes;
    this.totalNetUtilizationSource.push(objNet);

    /* Temperature Shit */
    const objThm = { };
    objThm['Time'] = this.currentTime;
    objThm[data.hostname] = data.cpuTemperature;
    this.totalThmUtilizationSource.push(objThm);

    this.removeIfNecessary(); // Clean the charts...

    /* Refresh The charts */
    this.cpuChart.refresh();
    this.memChart.refresh();
    this.netChart.refresh();
    this.thmChart.refresh();
  }

}
