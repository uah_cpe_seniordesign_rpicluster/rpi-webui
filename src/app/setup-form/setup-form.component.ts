import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {LocalStorageService} from 'ngx-localstorage';
import {ConfigurationService} from '../configuration.service';
import {DataService} from '../data.service';
import {jqxDropDownListComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import {jqxInputComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxinput';
import {jqxButtonComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbuttons';

@Component({
  selector: 'app-setup-form',
  templateUrl: './setup-form.component.html',
  styleUrls: ['./setup-form.component.css']
})
export class SetupFormComponent implements AfterViewInit, OnInit {

  @ViewChild('themeDropdown') themeSelector: jqxDropDownListComponent;
  @ViewChild('hostInp') hostInput: jqxInputComponent;
  @ViewChild('portInp') portInput: jqxInputComponent;
  @ViewChild('submitButton') submitButton: jqxButtonComponent;

  public source: string[] = [
    'classic',
    'dark',
    'orange',
    'energyblue',
    'darkblue'
  ];

  public hostIp: string;
  public hostPort: string;
  public context: string;
  public activeTheme: string;

  constructor(private cookieService: LocalStorageService,
              private config: ConfigurationService,
              private dataService: DataService) {



  }

  private testServiceCallback(data) {
    if (data !== 'Hello World!') {
      // Oof
    } else {
      this.cookieService.set('datasrvIp', this.hostIp);
      this.cookieService.set('datasrvPort', this.hostPort);
      this.cookieService.set('theme', this.themeSelector.getItem(this.themeSelector.getSelectedIndex()).value);

      this.hostInput.value('');
      this.portInput.value('');
      this.submitButton.disabled(true);
      this.context = 'To apply changes, please reload the application.';
    }
  }

  ngAfterViewInit() {

  }

  ngOnInit() {
    this.activeTheme = this.config.getActiveTheme();
  }

  public onSubmit() {
    this.dataService.setHost(this.hostIp + ':' + this.hostPort);
    this.dataService.getHello().subscribe((data) => this.testServiceCallback(data));
  }

}
