import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ConfigurationService} from '../configuration.service';
import {DataService} from '../data.service';
import {jqxDropDownListComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import {jqxChartComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxchart';

@Component({
  selector: 'app-host-detail',
  templateUrl: './host-detail.component.html',
  styleUrls: ['./host-detail.component.css']
})
export class HostDetailComponent implements OnInit, AfterViewInit {

  @ViewChild('hostDropDown') hostSelect: jqxDropDownListComponent;
  @ViewChild('cpuCoreChart') cpuCoreChart: jqxChartComponent;
  @ViewChild('netPacketsChart') packetsChart: jqxChartComponent;
  @ViewChild('netBytesChart') bytesChart: jqxChartComponent;
  @ViewChild('voltageChart') voltageChart: jqxChartComponent;
  @ViewChild('currentChart') currentChart: jqxChartComponent;
  @ViewChild('ambientTempChart') ambTempChart: jqxChartComponent;

  public activeTheme: string;

  private rootSubscriber;

  private hostList: string[];
  private activeHost: string;

  private cpuMultiList: any[];
  private cpuCoresSource = [];

  private packetsSource = [];

  private bytesSource = [];

  private voltageSource = [];

  private currentSource = [];

  private ambientTempSource = [];

  constructor(private config: ConfigurationService,
              private data: DataService) {
    this.hostList = [];
    this.activeHost = '';
    this.cpuMultiList = [];
  }

  ngOnInit() {
    this.activeTheme = this.config.getActiveTheme();
  }

  ngAfterViewInit() {
    this.cpuCoreChart.source(this.cpuCoresSource);
    this.cpuCoreChart.seriesGroups([ this.getPerCoreChartOptions() ]);

    this.packetsChart.source(this.packetsSource);
    this.packetsChart.seriesGroups([ this.getPacketChartOptions() ]);

    this.bytesChart.source(this.bytesSource);
    this.bytesChart.seriesGroups([ this.getBytesChartOptions() ]);

    this.voltageChart.source(this.voltageSource);
    this.voltageChart.seriesGroups([ this.getVoltageChartOptions() ]);

    this.currentChart.source(this.currentSource);
    this.currentChart.seriesGroups([ this.getCurrentChartOptions() ]);

    this.ambTempChart.source(this.ambientTempSource);
    this.ambTempChart.seriesGroups([ this.getAmbChartOptions() ]);
  }

  public start() {
    console.log('starting subscriber for host-detail polling.')
    this.rootSubscriber = this.data.poll$.subscribe((list) => this.populateDropDown(list));
  }

  public end() {
    console.log('stopping subscriber for host-detail polling.')
    this.rootSubscriber.unsubscribe();
  }

  private getCurrentTimeString(): string {
    const d = new Date();
    return d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
  }

  private populateDropDown(list) {
    // Update the hosts list.
    for (let i = 0; i < list.length; i++) {
      if (this.hostList.indexOf(list[i]) === -1) {
        this.hostSelect.addItem(list[i]);
        this.hostList.push(list[i]);
      }
    }

    this.chartUpdater();
  }

  private chartUpdater() {
    if (this.activeHost === '')
      return;

    this.data.getHostData(this.activeHost).subscribe((data) => this.chart(data));
  }

  public selectAction() {
    const host = this.hostSelect.getSelectedItem().label;
    this.activeHost = host;
  }

  private getPerCoreChartOptions() {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        maxValue: 100,
        minValue: 0,
        valuesOnTicks: true,
        description: '% CPU Utilization'
      },
      series: this.cpuMultiList
    };
  }

  private getPacketChartOptions() {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        minValue: 0,
        valuesOnTicks: true,
        description: 'Packets'
      },
      series: [{
        dataField: 'Tx',
        displayText: 'Tx',
        emptyPointsDisplay: 'connect'
      }, {
        dataField: 'Rx',
        displayText: 'Rx',
        emptyPointsDisplay: 'connect'
      }]
    };
  }

  private getBytesChartOptions() {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        minValue: 0,
        valuesOnTicks: true,
        description: 'kB/s'
      },
      series: [{
        dataField: 'Tx',
        displayText: 'Tx',
        emptyPointsDisplay: 'connect'
      }, {
        dataField: 'Rx',
        displayText: 'Rx',
        emptyPointsDisplay: 'connect'
      }]
    };
  }

  private getVoltageChartOptions() {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        minValue: 0,
        valuesOnTicks: true,
        description: 'Volts (V)'
      },
      series: [{
        dataField: 'backplane',
        displayText: 'Backplane Voltage',
        emptyPointsDisplay: 'connect'
      }, {
        dataField: 'processor',
        displayText: 'Processor Voltage',
        emptyPointsDisplay: 'connect'
      }]
    };
  }

  private getCurrentChartOptions() {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        minValue: 0,
        valuesOnTicks: true,
        description: 'Current'
      },
      series: [{
        dataField: 'module',
        displayText: 'Module Current Draw',
        emptyPointsDisplay: 'connect'
      }]
    };
  }

  private getAmbChartOptions() {
    return {
      type: 'line',
      orientation: 'vertical',
      xAxis: {
        dataField: 'Time',
        description: 'Timestamp'
      },
      valueAxis: {
        minValue: 0,
        valuesOnTicks: true,
        description: 'Temperature (C)'
      },
      series: [{
        dataField: 'ambTemp',
        displayText: 'Ambient Temperature',
        emptyPointsDisplay: 'connect'
      }]
    };
  }


  private isAlreadyThere(host: string): boolean {
    for (let i = 0; i < this.cpuMultiList.length; i++) {
      const tempObj = this.cpuMultiList[i];
      if (tempObj.dataField === host) {
        return true;
      }
    }
    return false; // If not returned by now, must be false
  }

  private removeIfNecessary() {

    if (this.cpuCoresSource.length < 60) {
      return;
    }
    this.cpuCoresSource.splice(0, 1);
    this.packetsSource.splice(0, 1);
    this.bytesSource.splice(0, 1);
    this.voltageSource.splice(0, 1);
    this.currentSource.splice(0, 1);
    this.ambientTempSource.splice(0, 1);
  }

  async chart(data) {
    const currentTime = this.getCurrentTimeString();

    const cpuArr = data.perCoreCpuUtilization;
    const cpuObj = { };
    cpuObj['Time'] = currentTime;
    for (let i = 1; i < cpuArr.length; i++) {
      if (!this.isAlreadyThere('cpu-' + i)) {
        this.cpuMultiList.push({
          dataField: 'cpu-' + i,
          displayText: 'cpu-' + i,
          emptyPointsDisplay: 'connect'
        });
      }
      cpuObj['cpu-' + i] = cpuArr[i];
    }
    this.cpuCoresSource.push(cpuObj);
    this.cpuCoreChart.refresh();

    const txP = data.txPackets;
    const rxP = data.rxPackets;

    this.packetsSource.push({
      Time: currentTime,
      Tx: txP,
      Rx: rxP
    });
    this.packetsChart.refresh();

    const txB = data.txBytes;
    const rxB = data.rxBytes;

    this.bytesSource.push({
      Time: currentTime,
      Tx: txB,
      Rx: rxB
    });
    this.bytesChart.refresh();

    const cpuV = data.processorVoltage;
    const backV = data.backplaneVoltage;

    this.voltageSource.push({
      Time: currentTime,
      backplane: backV,
      processor: cpuV
    });
    this.voltageChart.refresh();

    const modC = data.moduleCurrentDraw;

    this.currentSource.push({
      Time: currentTime,
      module: modC
    });
    this.currentChart.refresh();

    const amb = data.ambientTemp;

    this.ambientTempSource.push({
      Time: currentTime,
      ambTemp: amb
    });
    this.ambTempChart.refresh();

    this.removeIfNecessary();
  }
}
