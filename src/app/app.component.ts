import {Component, OnInit, ViewChild, ViewRef} from '@angular/core';
import {ConfigurationService} from './configuration.service';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HostDetailComponent} from './host-detail/host-detail.component';
import {jqxTabsComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'rpi-webui';

  @ViewChild('tabWidget') tabWidget: jqxTabsComponent;
  @ViewChild('dashboard') dash: DashboardComponent;
  @ViewChild('hostDetail') hostDetail: HostDetailComponent;

  public activeTheme: string;

  constructor(private config: ConfigurationService) {

  }

  ngOnInit() {
    this.activeTheme = this.config.getActiveTheme();
  }

  public selectAction() {
    const tabInd = this.tabWidget.selectedItem();
    console.log(this.tabWidget.selectedItem());

    switch (tabInd) {
      case 0: { // Dashboard
        this.dash.start();
        this.hostDetail.end();
        break;
      }
      case 1: { // Host detail
        this.dash.end();
        this.hostDetail.start();
        break;
      }
      case 2: {
        this.dash.end();
        this.hostDetail.end();
        break;
      }
      default: {
        console.log('oof. tab is wrong...');
      }
    }
  }
}
