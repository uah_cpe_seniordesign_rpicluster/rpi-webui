import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SetupFormComponent } from './setup-form/setup-form.component';
import { HostDetailComponent } from './host-detail/host-detail.component';
import { SetupWindowComponent } from './setup-window/setup-window.component';
import {FormsModule} from '@angular/forms';
import {NgxLocalStorageModule} from 'ngx-localstorage';
import {HttpClient, HttpClientModule, HttpHandler} from '@angular/common/http';

// jqWidgets import statements:
import { jqxTabsComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtabs';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxinput';
import { jqxButtonComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbuttons';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import {jqxChartComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxchart';
import {jqxSplitterComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxsplitter';


@NgModule({
  declarations: [
    AppComponent,
    jqxTabsComponent,
    jqxWindowComponent,
    jqxInputComponent,
    jqxButtonComponent,
    jqxDropDownListComponent,
    jqxChartComponent,
    jqxSplitterComponent,
    DashboardComponent,
    SetupFormComponent,
    HostDetailComponent,
    SetupWindowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxLocalStorageModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
